﻿using BookStoreWebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStoreWebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {

        BookStoreDBContext DbContext;
        public WeatherForecastController() {
            DbContext = new BookStoreDBContext();
        }

        [HttpGet]
        public IEnumerable<Product> GetAllProducts()
        {
            //using(var _context = new BookStoreDBContext())
            // {
            //Publisher publisher = new Publisher();
            //publisher.PublisherName = "Hari krishna";

            //_context.Publishers.Add(publisher);

            //_context.SaveChanges();

            //Publisher publisher = _context.Publishers.FirstOrDefault();
            //publisher.PublisherName = "Hari krishna nagothu";

            //_context.Publishers.Remove(publisher);

            // _context.SaveChanges();

            //Product product = new Product();
            //product.ProductName = "Dell laptop#%#$#%#$";
            //product.Quantity = 1;
            //product.Price = 10000;

            //DbContext.Products.Add(product);
            //DbContext.SaveChanges();
            return DbContext.Products.ToList();
            //return DbContext.Products.ToList();
                //TO Do git lab commands
           // }

            
        }
        //[HttpGet]
        //public IEnumerable<Product> GetProduct(int Id)
        //{
        //    return DbContext.Products.Where(p => p.ProductId == Id);
        //}

        [HttpPost]
        public IEnumerable<Product> AddProduct()
        {
            Product product = new Product();
            product.ProductName = "Lenovo laptop";
            product.Quantity = 1;
            product.Price = 10000;

            DbContext.Products.Add(product);
            DbContext.SaveChanges();
            return DbContext.Products.ToList(); 

        }
    }
}
